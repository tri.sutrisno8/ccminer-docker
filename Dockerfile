FROM ubuntu:22.04
MAINTAINER "Origin by Hiroki Takeyama github"

# timezone
RUN echo "apt process"
RUN apt update && apt install -y tzdata curl libcurl4-openssl-dev libssl-dev libjansson-dev proxychains tmux autossh; \
    apt clean;
	
# proxychains
COPY proxychains.conf /etc/proxychains.conf

# ccminer
RUN echo "setup ccminer"
WORKDIR /root

#COPY --chmod=777 ccmineramd /bin/ccminer
COPY ccmineramd /bin/ccminer
RUN chmod 777 /bin/ccminer

# COPY --chmod=600 whplus /usr/local/bin/
COPY whplus /usr/local/bin/
RUN chmod 600 /usr/local/bin/whplus
# COPY --chmod=600 whplus /root/
COPY whplus /root/
RUN chmod 600 /root/whplus

# entrypoint
RUN echo "create entry_point"
RUN { \
    echo '#!/bin/bash -eu'; \
    echo 'ln -fs /usr/share/zoneinfo/${TZ} /etc/localtime'; \
	echo 'tmux new-session -d -s tunnelwhplus "autossh -4 -i whplus -o StrictHostKeyChecking=no -p 14920 -D 14905 -N root@103.174.234.120"'; \
	echo 'proxychains ccminer -a verus -o ${POOLURL} -u ${WALLET} -p x -t ${NCPU}'; \
    # echo 'echo "root:${ROOT_PASSWORD}" | chpasswd'; \
	# echo 'service ssh start'; \
    # echo 'ccminer -a verus -o ${POOLURL} -u ${WALLET} -p x -t ${NCPU}'; \
    echo 'exec "$@"'; \
    } > /usr/local/bin/entry_point.sh; \
    chmod +x /usr/local/bin/entry_point.sh;


ENV TZ Asia/Jakarta

ENV ROOT_PASSWORD root
WORKDIR /root

ENTRYPOINT ["entry_point.sh"]
CMD    [""]